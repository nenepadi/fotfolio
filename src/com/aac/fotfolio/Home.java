/**
 * 
 */
package com.aac.fotfolio;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * @author kizito
 *
 */
public class Home extends Activity implements View.OnClickListener {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		
		initialize();
	}
	
	void initialize(){
		Button btnLog = (Button) findViewById(R.id.btnLogin);
		Button btnSignUp = (Button) findViewById(R.id.btnSignUp);
		Button btnAbout = (Button) findViewById(R.id.btnAbout);
		btnLog.setOnClickListener(this);
		btnSignUp.setOnClickListener(this);
		btnAbout.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.btnLogin:
				Intent goLogin = new Intent(Home.this, Login.class);
				startActivity(goLogin);
				break;
			case R.id.btnSignUp:
				Intent goSign = new Intent(Home.this, SignUp.class);
				startActivity(goSign);
				break;
			case R.id.btnAbout:
				Intent goAbout = new Intent(Home.this, About.class);
				startActivity(goAbout);
				break;
		}
		
	}
}
